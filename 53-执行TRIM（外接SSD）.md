# 执行TRIM（外接SSD）

- [执行TRIM（外接SSD）](#执行trim外接ssd)
  - [1. SSD 上的 TRIM 命令](#1-ssd-上的-trim-命令)
  - [2. Windows 上的 TRIM](#2-windows-上的-trim)
  - [3. 手动运行 TRIM 或更改自动计划](#3-手动运行-trim-或更改自动计划)
  - [4. 使用命令手动运行 TRIM](#4-使用命令手动运行-trim)


## 1. SSD 上的 TRIM 命令

> TRIM 是 ATA（高级技术指令）命令。当操作系统确定某个数据块将不再被使用时，它会对该数据块执行完全擦除。此 TRIM 命令允许 SSD 控制器更有效地管理 SSD 上的可用空间。因此，TRIM 命令可以提高 SSD 的性能并延长其使用寿命。

## 2. Windows 上的 TRIM 

在 Windows 10 中，您可以在具有受支持的主机控制器、设备和文件系统的内部 SSD 和 USB 连接 SSD 上使用 TRIM 命令。 Windows 10 支持 UASP（USB 连接 SCSI 协议），并支持具有 NTFS 或 ReFS 文件系统的 USB 存储设备上的 TRIM。 请注意，某些早期的 USB 3.0 主控制器无法正确支持 UASP 或 TRIM。

您可以按照以下步骤检查 Windows 10 中是否启用了 TRIM：

1. 以管理员权限启动 Windows PowerShell（右键单击“开始”菜单并选择“Windows PowerShell（管理员）”）

2. Windows 用户帐户控制允许应用程序以管理员身份运行

3. 输入以下命令检查“DisableDeleteNotify”的值

   ```powershell
   fsutil behavior query DisableDeleteNotify
   ```

4. 如果“DisableDeleteNotify”的值为1，则通过运行以下命令启用TRIM：

   ```powershell
   fsutil behavior set DisableDeleteNotify 0
   ```

## 3. 手动运行 TRIM 或更改自动计划

1. 打开 Windows 10 开始菜单并在搜索框中键入“优化”
2. 从搜索结果中选择“碎片整理和优化驱动器”应用程序
3. “优化驱动器”窗口显示可进行碎片整理的已连接驱动器
4. 从列表中选择USB连接的SSD，然后按“优化”按钮手动启动TRIM
5. 如有必要，请按“更改设置”按钮调整驱动器上的 TRIM 或计划优化。

## 4. 使用命令手动运行 TRIM

1. 以管理员权限启动 Windows PowerShell（右键单击“开始”菜单并选择“Windows PowerShell（管理员）”）

2. Windows 用户帐户控制允许应用程序以管理员身份运行

3. 执行以下命令。对于“X:”，输入要执行 TRIM 的驱动器名称。

   ```powershell
   defrag /o X:
   ```

   