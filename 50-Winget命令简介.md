# winget 命令简介

## 目录

[[_TOC_]]

## 1. winget命令概要

- upgrade

  https://docs.microsoft.com/zh-cn/windows/package-manager/winget/upgrade

- install

  https://docs.microsoft.com/zh-cn/windows/package-manager/winget/install

- import

  https://docs.microsoft.com/zh-cn/windows/package-manager/winget/import

- export

  https://docs.microsoft.com/zh-cn/windows/package-manager/winget/export

- tab 自动补全

  https://docs.microsoft.com/zh-cn/windows/package-manager/winget/tab-completion

[winget](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/) 工具的 upgrade 命令可升级指定的应用程序。 或者，也可使用 [list](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/list) 命令来识别要升级的应用程序。

upgrade 命令要求你指定要升级内容的具体字符串。 如果存在任何不明确性，系统会提示你进一步将 upgrade 命令筛选到具体应用程序。

## 2. 指令说明

```
winget upgrade [[-q] \<query>] [\<options>]
```

![Image of upgrade command arguments](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/images/upgrade.png)

## 3. 指令参数

可使用以下参数。

| 参数            | 说明                       |
| :-------------- | :------------------------- |
| **-q、--query** | 用于搜索应用的查询。       |
| **-?、--help**  | 获取有关此命令的更多帮助。 |

 备注

查询参数是位置参数。 不支持通配符样式语法。 这通常是你期望帮助查找所升级的程序包的字符串。

## 4. 指令选项

这些选项让你可以根据自己的需求自定义升级体验。

| 选项                        | 说明                                                         |
| :-------------------------- | :----------------------------------------------------------- |
| **-m、--manifest**          | 必须后跟清单 (YAML) 文件的路径。 你可使用清单从[本地 YAML 文件](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/install#local-install)运行升级体验。 |
| **--id**                    | 将升级限制为应用程序的 ID。                                  |
| **--name**                  | 将搜索限制为应用程序的名称。                                 |
| **--moniker**               | 将搜索限制为针对应用程序列出的名字对象。                     |
| **-v、--version**           | 让你可以指定要升级的确切版本。 如果未指定，则使用 latest 会升级到应用程序的最高版本。 |
| **-s、--source**            | 将搜索限制为所提供的源名称。 必须后跟源名称。                |
| **-e、--exact**             | 在查询中使用确切的字符串，包括检查是否区分大小写。 它不会使用子字符串的默认行为。 |
| **-i、--interactive**       | 以交互模式运行安装程序。 默认体验会显示安装程序进度。        |
| **-h、--silent**            | 以静默模式运行安装程序。 此选项禁止显示所有 UI。 默认体验会显示安装程序进度。 |
| **-o、--log**               | 将日志记录定向到日志文件。 必须提供你具有写入权限的文件的路径。 |
| **--override**              | 要直接传递给安装程序的字符串。                               |
| **-l、--location**          | 要升级到的位置（如果支持）。                                 |
| **--force**                 | 如果发现哈希不匹配，则将忽略错误并尝试安装包。               |
| --accept-package-agreements | 用于接受许可协议，并避免出现提示。                           |
| --accept-source-agreements  | 用于接受源许可协议，并避免出现提示。                         |
| --header                    | 可选 Windows-Package-Manager REST 源 HTTP 标头。             |
| **--all**                   | 将所有可用的包更新为最新的应用程序。                         |
| **--include-unknown**       | 升级包（即使无法确定其当前版本）。                           |
| **--verbose-logs**          | 用于替代日志记录设置并创建详细日志。                         |

### 4.1 查询示例

以下示例将升级特定版本的应用程序。

CMD复制

```CMD
winget upgrade powertoys --version 0.15.2
```

以下示例将根据应用程序 ID 升级相应的应用程序。

CMD复制

```CMD
winget upgrade --id Microsoft.PowerToys
```

以下示例演示如何升级所有应用

CMD复制

```CMD
winget upgrade --all
```

## 5. 使用 list 和 upgrade

通常使用 [list](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/list) 命令识别需要更新的应用，然后使用 upgrade 安装最新的应用 。

在下面的示例中，你将看到 [list](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/list) 识别出 Microsoft.WindowsTerminalPreview 有一个可用更新，然后用户使用 upgrade 来更新该应用程序 。

![Animation demonstrating list and upgrade commands](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/images/upgrade.gif)

### 5.1 upgrade --all

upgrade --all 将识别有可用升级的所有应用程序。 运行 winget upgrade --all 时，Windows 程序包管理器将查找具有可用更新的所有应用程序并尝试安装更新。

**备注：**某些应用程序不提供版本。 它们始终是最新的。 由于 Windows 程序包管理器无法识别是否有较新版本的应用，因此无法进行升级。

------

## 6. 建议的内容

- [install 命令](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/install)

  安装指定的应用程序。

- [search 命令](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/search)

  查询可用于安装的应用程序的源

- [list 命令](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/list)

  显示列出应用的列表以及是否有可用更新。

- [uninstall 命令](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/uninstall)

  卸载指定的应用程序。

显示更多

本文内容

- [使用情况](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/upgrade#usage)
- [参数](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/upgrade#arguments)
- [选项](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/upgrade#options)
- [使用 list 和 upgrade](https://docs.microsoft.com/zh-cn/windows/package-manager/winget/upgrade#using-list-and-upgrade)

显示更多

[中文 (简体)](https://docs.microsoft.com/zh-cn/locale?target=https://docs.microsoft.com/zh-cn/windows/package-manager/winget/upgrade)

主题

 