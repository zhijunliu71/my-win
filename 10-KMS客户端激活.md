# KMS 客户端激活

> https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81

若要使用 KMS，需要有一个在本地网络上可用的 KMS 主机。 使用 KMS 主机激活的计算机需要具有特定的产品密钥。 此密钥有时称为 KMS 客户端密钥，但其正式名称为 Microsoft 通用批量许可证密钥 (GVLK)。 默认情况下，运行 Windows Server 和 Windows 客户端批量许可版本的计算机是无需额外配置的 KMS 客户端，因为相关 GVLK 已经存在。

但是，在某些情况下，需要将 GVLK 添加到要针对 KMS 主机激活的计算机，例如：

- 转换计算机使其不使用多次激活密钥 (MAK)
- 将 Windows 的零售许可证转换为 KMS 客户端
- 如果计算机以前是 KMS 主机

> [!NOTE]  
> 若要使用此处列出的密钥（它们是 GVLK），你必须首先在本地网络中拥有可用的 KMS 主机。 如果还没有 KMS 主机，请参阅如何创建 KMS 主机以了解详细信息。
>
> 如果希望在没有可用的 KMS 主机的情况下在批量激活方案之外激活 Windows（例如，尝试激活 Windows 客户端的零售版本），则这些密钥将不起作用。 需要使用另一种激活 Windows 的方法，如使用 MAK 或购买零售许可证。 获取帮助以查找 Windows 产品密钥并了解 Windows 的正版版本。

## 安装产品密钥

如果要将计算机从 KMS 主机、MAK 或零售版本 Windows 转换为 KMS 客户端，可以从本文中的列表安装适用的产品密钥 (GVLK)。 若要安装客户端产品密钥，请在客户端上打开一个管理命令提示符，并运行以下命令，然后按 `Enter`：

Windows 命令提示符

```cmd
slmgr /ipk <product key>
```

例如，若要安装 Windows Server 2022 Datacenter 版的产品密钥，请运行以下命令，然后按 `Enter`：

Windows 命令提示符

```cmd
slmgr /ipk WX4NM-KYWYW-QJJR4-XV3QB-6VM33
```

## 通用批量许可证密钥

在下表中，可找到 Windows 每个版本的 GVLK。 LTSC 是长期服务渠道，而 LTSB 是 Long-Term Servicing Branch 。



### Windows Server LTSC

- [Windows Server 2025](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_1_server2025)

  | 操作系统版本                                  | KMS 客户端产品密钥            |
  | :-------------------------------------------- | :---------------------------- |
  | Windows Server 2025 标准                      | TVRH6-WHNXV-R9WG3-9XRFY-MY832 |
  | Windows Server 2025 数据中心                  | D764K-2NDRG-47T6Q-P8T8W-YP6DF |
  | Windows Server 2025 Datacenter：Azure Edition | XGN3F-F394H-FD2MY-PP6FD-8MCRC |
- [Windows Server 2022](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_1_server2022)

  | 操作系统版本                                  | KMS 客户端产品密钥            |
  | :-------------------------------------------- | :---------------------------- |
  | Windows Server 2022 Standard                  | VDYBN-27WPP-V4HQT-9VMD4-VMK7H |
  | Windows Server 2022 Datacenter                | WX4NM-KYWYW-QJJR4-XV3QB-6VM33 |
  | Windows Server 2022 Datacenter：Azure Edition | NTBV8-9K7Q8-V27C6-M2BTV-KHMXV |
- [Windows Server 2019](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_1_server2019)

  | 操作系统版本                   | KMS 客户端产品密钥            |
  | :----------------------------- | :---------------------------- |
  | Windows Server 2019 Standard   | N69G4-B89J2-4G8F4-WWYCC-J464C |
  | Windows Server 2019 Datacenter | WMDGN-G9PQG-XVVXX-R3X43-63DFG |
  | Windows Server 2019 Essentials | WVDHN-86M7X-466P6-VHXV7-YY726 |
- [Windows Server 2016](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_1_server2016)

  | 操作系统版本                   | KMS 客户端产品密钥            |
  | :----------------------------- | :---------------------------- |
  | Windows Server 2016 Standard   | WC2BQ-8NRM3-FDDYY-2BFGV-KHKQY |
  | Windows Server 2016 Datacenter | CB7KF-BWN84-R7R2Y-793K2-8XDDG |
  | Windows Server 2016 Essentials | JCKRF-N37P4-C2D82-9YXRT-4M63B |

### Windows Server 半年频道

- Windows Server，版本 20H2、2004、1909、1903 和 1809

  | 操作系统版本              | KMS 客户端产品密钥            |
  | :------------------------ | :---------------------------- |
  | Windows Server Standard   | N2KJX-J94YW-TQVFB-DG9YT-724CC |
  | Windows Server Datacenter | 6NMRW-2C8FM-D24W7-TQWMY-CWH2D |

  > [!NOTE] 重要
  >
  > Windows Server 版本 20H2 已于 2022 年 8 月 9 日终止服务，不再接收安全更新。 这包括停用 Windows Server 半年频道 (SAC)，不再提供未来版本。
  >
  > 使用 Windows Server SAC 的客户应迁移到 [Azure Stack HCI](https://learn.microsoft.com/zh-cn/azure-stack/hci/)。 或者，客户可使用 Windows Server 的长期服务渠道。



### Windows 11 和 Windows 10（半年频道）

- 有关受支持的版本和服务终止日期的信息，请参阅 [Windows 生命周期情况说明书](https://support.microsoft.com/help/13853/windows-lifecycle-fact-sheet)。

  | 操作系统版本                                        | KMS 客户端产品密钥            |
  | :-------------------------------------------------- | :---------------------------- |
  | Windows 11 专业版 Windows 10 专业版                 | W269N-WFGWX-YVC9B-4J6C9-T83GX |
  | Windows 11 专业版 N Windows 10 专业版 N             | MH37W-N47XK-V7XM9-C7227-GCQG9 |
  | Windows 11 专业工作站版 Windows 10 专业工作站版     | NRG8B-VKK3Q-CXVCJ-9G2XF-6Q84J |
  | Windows 11 专业工作站版 N Windows 10 专业工作站版 N | 9FNHH-K3HBT-3W4TD-6383H-6XYWF |
  | Windows 11 专业教育版 Windows 10 专业教育版         | 6TP4R-GNPTD-KYYHQ-7B7DP-J447Y |
  | Windows 11 专业教育版 N Windows 10 专业教育版 N     | YVWGF-BXNMC-HTQYQ-CPQ99-66QFC |
  | Windows 11 教育版 Windows 10 教育版                 | NW6C2-QMPVW-D7KKK-3GKT6-VCFB2 |
  | Windows 11 教育版 N Windows 10 教育版 N             | 2WH4N-8QGBV-H22JP-CT43Q-MDWWJ |
  | Windows 11 企业版 Windows 10 企业版                 | NPPR9-FWDCX-D2C8J-H872K-2YT43 |
  | Windows 11 企业版 N Windows 10 企业版 N             | DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4 |
  | Windows 11 企业版 G Windows 10 企业版 G             | YYVX9-NTFWV-6MDM3-9PT4T-4M68B |
  | Windows 11 企业版 G N Windows 10 企业版 G N         | 44RPN-FTY23-9VTTB-MP9BX-T84FV |



### Windows 企业版 LTSC 和 LTSB

- [Windows 11 LTSC 2024 Windows 10 LTSC 2021、2019](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_2_windows1110ltsc)

  | 操作系统版本                                                 | KMS 客户端产品密钥            |
  | :----------------------------------------------------------- | :---------------------------- |
  | Windows 11 企业版 LTSC 2024 Windows 10 企业版 LTSC 2021 Windows 10 企业版 LTSC 2019 | M7XTQ-FN8P6-TTKYV-9D4CC-J462D |
  | Windows 11 企业版 N LTSC 2024 Windows 10 企业版 N LTSC 2021 Windows 10 企业版 N LTSC 2019 | 92NFX-8DJQP-P6BBQ-THF9C-7CG2H |
- [Windows IoT LTSC](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_2_winowsiot)

  | 操作系统版本                                              | KMS 客户端产品密钥            |
  | :-------------------------------------------------------- | :---------------------------- |
  | Windows IoT 企业版 LTSC 2024 Windows IoT 企业版 LTSC 2021 | KBN8V-HFGQ4-MGXVD-347P6-PDQGT |

  > [!WARNING] 备注
  >
  > 对于 ImageVersion：10.0.19044.2905 或更高版本，请参阅[批量许可证中的 Windows IoT 企业版 LTSC](https://learn.microsoft.com/zh-cn/windows/iot/iot-enterprise/deployment/volume-license)。
- [Windows 10 LTSB 2016](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_2_windows102016)

  | 操作系统版本                  | KMS 客户端产品密钥            |
  | :---------------------------- | :---------------------------- |
  | Windows 10 企业版 LTSB 2016   | DCPHK-NFMTC-H88MJ-PFHPY-QJ4BJ |
  | Windows 10 企业版 N LTSB 2016 | QFFDN-GRT3P-VKWWX-X7T3R-8B639 |
- [Windows 10 LTSB 2015](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_2_windows102015)

  | 操作系统版本                  | KMS 客户端产品密钥            |
  | :---------------------------- | :---------------------------- |
  | Windows 10 企业版 LTSB 2015   | WNMTR-4C88C-JK8YV-HQ7T2-76DF9 |
  | Windows 10 企业版 N LTSB 2015 | 2F77B-TNFGY-69QQF-B8YKP-D69TJ |

### 早期版本的 Windows Server

- [Windows Server 版本 1803](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_3_version1803)

  | 操作系统版本              | KMS 客户端产品密钥            |
  | :------------------------ | :---------------------------- |
  | Windows Server Standard   | PTXN8-JFHJM-4WC78-MPCBR-9W4KR |
  | Windows Server Datacenter | 2HXDN-KRXHB-GPYC7-YCKFJ-7FVDG |
- [Windows Server 版本 1709](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_3_version1709)

  | 操作系统版本              | KMS 客户端产品密钥            |
  | :------------------------ | :---------------------------- |
  | Windows Server Standard   | DPCNP-XQFKJ-BJF7R-FRC8D-GF6G4 |
  | Windows Server Datacenter | 6Y6KB-N82V8-D8CQV-23MJW-BWTG6 |
- [Windows Server 2012 R2](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_3_2012r2)

  | 操作系统版本                      | KMS 客户端产品密钥            |
  | :-------------------------------- | :---------------------------- |
  | Windows Server 2012 R2 Standard   | D2N9P-3P6X9-2R39C-7RTCD-MDVJX |
  | Windows Server 2012 R2 Datacenter | W3GGN-FT8W3-Y4M27-J84CP-Q3VJ9 |
  | Windows Server 2012 R2 Essentials | KNC87-3J2TX-XB4WP-VCPJV-M4FWM |
- [Windows Server 2012](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_3_2012)

  | 操作系统版本                          | KMS 客户端产品密钥            |
  | :------------------------------------ | :---------------------------- |
  | Windows Server 2012                   | BN3D2-R7TKB-3YPBD-8DRP2-27GG4 |
  | Windows Server 2012 N                 | 8N2M2-HWPGY-7PGT9-HGDD8-GVGGY |
  | Windows Server 2012 单语言版          | 2WN2H-YGCQR-KFX6K-CD6TF-84YXQ |
  | Windows Server 2012 特定国家/地区版   | 4K36P-JN4VD-GDC6V-KDT89-DYFKP |
  | Windows Server 2012 Standard          | XC9B7-NBPP2-83J2H-RHMBY-92BT4 |
  | Windows Server 2012 MultiPoint 标准版 | HM7DN-YVMH3-46JC3-XYTG7-CYQJJ |
  | Windows Server 2012 MultiPoint 高级版 | XNH6W-2V9GX-RGJ4K-Y8X6F-QGJ2G |
  | Windows Server 2012 Datacenter        | 48HP8-DN98B-MYWDG-T2DCC-8W83P |
  | Windows Server 2012 Essentials        | HTDQM-NBMMG-KGYDT-2DTKT-J2MPV |
- [Windows Server 2008 R2](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_3_2008r2)

  | 操作系统版本                                   | KMS 客户端产品密钥            |
  | :--------------------------------------------- | :---------------------------- |
  | Windows Server 2008 R2 Web 版                  | 6TPJF-RBVHG-WBW2R-86QPH-6RTM4 |
  | Windows Server 2008 R2 HPC 版                  | TT8MH-CG224-D3D7Q-498W2-9QCTX |
  | Windows Server 2008 R2 标准版                  | YC6KT-GKW9T-YTKYR-T4X34-R7VHC |
  | Windows Server 2008 R2 企业版                  | 489J6-VHDMP-X63PK-3K798-CPX3Y |
  | Windows Server 2008 R2 Datacenter              | 74YFP-3QFB3-KQT8W-PMXWJ-7M648 |
  | 面向基于 Itanium 系统的 Windows Server 2008 R2 | GT63C-RJFQ3-4GMB6-BRFB9-CB83V |
- [Windows Server 2008](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_3_2008)

  | 操作系统版本                                   | KMS 客户端产品密钥            |
  | :--------------------------------------------- | :---------------------------- |
  | Windows Web Server 2008                        | WYR28-R7TFJ-3X2YQ-YCY4H-M249D |
  | Windows Server 2008 标准版                     | TM24T-X9RMF-VWXK6-X8JC9-BFGM2 |
  | 不带 Hyper-V 的 Windows Server 2008 标准版     | W7VD6-7JFBR-RX26B-YKQ3Y-6FFFJ |
  | Windows Server 2008 企业版                     | YQGMW-MPWTJ-34KDK-48M3W-X4Q6V |
  | 不带 Hyper-V 的 Windows Server 2008 企业版     | 39BXF-X8Q23-P2WWT-38T2F-G3FPG |
  | Windows Server 2008 HPC                        | RCTX3-KWVHP-BR6TB-RB6DM-6X7HP |
  | Windows Server 2008 Datacenter                 | 7M67G-PC374-GR742-YH8V4-TCBY3 |
  | 不带 Hyper-V 的 Windows Server 2008 数据中心版 | 22XQ2-VRXRG-P8D42-K34TD-G3QQC |
  | 面向基于 Itanium 系统的 Windows Server 2008    | 4DWFP-JF3DJ-B7DTH-78FJB-PDRHK |

### 早期版本的 Windows 客户端

- [Windows 8.1](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_4_windows81)

  | 操作系统版本         | KMS 客户端产品密钥            |
  | :------------------- | :---------------------------- |
  | Windows 8.1 专业版   | GCRJD-8NW9H-F2CDX-CCM8D-9D6T9 |
  | Windows 8.1 专业版 N | HMCNV-VVBFX-7HMBH-CTY9B-B4FXY |
  | Windows 8.1 企业版   | MHF9N-XY6XB-WVXMC-BTDCT-MKKG7 |
  | Windows 8.1 企业版 N | TT4HM-HN7YT-62K67-RGRQJ-JFFXW |
- [Windows 8](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_4_windows8)

  | 操作系统版本       | KMS 客户端产品密钥            |
  | :----------------- | :---------------------------- |
  | Windows 8 专业版   | NG4HW-VH26C-733KW-K6F98-J8CK4 |
  | Windows 8 专业版 N | XCVCF-2NXM9-723PB-MHCB7-2RYQQ |
  | Windows 8 企业版   | 32JNW-9KQ84-P47T8-D8GGY-CWCK7 |
  | Windows 8 企业版 N | JMNMF-RHW7P-DMY6X-RF3DR-X2BQT |
- [Windows 7](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_4_windows7)

  | 操作系统版本       | KMS 客户端产品密钥            |
  | :----------------- | :---------------------------- |
  | Windows 7 专业版   | FJ82H-XT6CR-J8D7P-XQJJ2-GPDD4 |
  | Windows 7 专业版 N | MRPKT-YTG23-K7D7T-X2JMM-QY7MG |
  | Windows 7 专业版 E | W82YF-2Q76Y-63HXB-FGJG9-GF7QX |
  | Windows 7 企业版   | 33PXH-7Y6KF-2VJC9-XBBR8-HVTHH |
  | Windows 7 企业版 N | YDRBP-3D83W-TY26F-D46B2-XCKRJ |
  | Windows 7 企业版 E | C29WB-22CC8-VJ326-GHFJW-H9DH4 |
- [Windows Vista](https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81#tabpanel_4_windowsvista)

  | 操作系统版本           | KMS 客户端产品密钥            |
  | :--------------------- | :---------------------------- |
  | Windows Vista 商用版   | YFKBB-PQJJV-G996G-VWGXY-2V3X8 |
  | Windows Vista 商用版 N | HMBQG-8H2RH-C77VX-27R82-VMQBT |
  | Windows Vista 企业版   | VKK3X-68KWM-X2YGT-QR4M6-4BWMV |
  | Windows Vista 企业版 N | VTC42-BM838-43QHV-84HX6-XJXKV |
