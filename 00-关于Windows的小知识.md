# 关于Windows系统相关知识

## 目录

[[_TOC_]]

## 1. 如何查看win10是否为正版

- 命令提示符里面输入 `slmgr.vbs -xpr` 

  ```
  slmgr.vbs -xpr
  ```


## 2. 禁止 Windows 进入休眠

- 尤其是虚拟机，不需要该功能

  ```shell
  powercfg.exe /hibernate off
  ```

## 3. Windows系统激活

### 3.1 激活方法-1

- Powershell（管理员）输入如下指令：

  ```bash
  irm https://massgrave.dev/get | iex
  ## 2024.08.31之后，请使用如下命令
  irm https://get.activated.win | iex
  ```

  再出现的选择画面里面，输入数字“1”选择即永久激活。

  ※ `家庭教育版`亲测可行，`专业版` `企业版` 的也是可行的，个人尝试也激活成功，极个别出现无法激活情况原因未知。

  **激活的时候，画面显示如下信息：**

  ```shell
  Checking OS Info                        [Windows 10 专业版 | 19045.3693 | AMD64]
  Checking Internet Connection            [Connected]
  Initiating Diagnostic Tests...
  Checking WPA Registry Count             [5]
  
  Installing Generic Product Key          [VK7JG-NPHTM-C97JM-9MPGT-3V66T] [Successful]
  Changing Windows Region To USA          [Successful]
  Generating GenuineTicket.xml            [Successful]
  Done.
  Converted license Microsoft.Windows.48.X19-98841_8wekyb3d8bbwe and stored at C:\ProgramData\Microsoft\Windows\ClipSvc\Install\Migration\585806c8-d531-4183-8fb0-32ab97d7f3da.xml.
  Successfully converted 1 licenses from genuine authorization tickets on disk.
  Done.
  
  Activating...
  
  Windows 10 专业版 is permanently activated with a digital license.
  
  Restoring Windows Region                [Successful]
  ```

  怀疑这个 `VK7JG-NPHTM-C97JM-9MPGT-3V66T` 是否是微软未公开的秘密注册码。

  **Office 2019 的激活信息：**

  ```shell
  Checking OS Info                        [Windows 10 Home | 19045.3758 | AM
  D64]
  Initiating Diagnostic Tests...
  Checking WPA Registry Count             [36]
  
  Activating Office 16.0 x86 C2R...
  Installing Generic Product Key          [BN4XJ-R9DYY-96W48-YK8DM-MY7P
  Y] [ProPlus2019Retail] [Retail] [Successful]
  
  Symlinking System's sppc.dll To         ["C:\Program Files (x86)\Microsoft 
  Office\root\vfs\SystemX86\sppcs.dll"] [Successful]
  Extracting Custom sppc32.dll To         ["C:\Program Files (x86)\Microsoft 
  Office\root\vfs\SystemX86\sppc.dll"] [Successful]
  Modifying Hash of Custom sppc32.dll     [Successful]
  Adding Reg Keys To Skip License Check   [Successful]
  
  Office is permanently activated.
  Help: https://massgrave.dev/troubleshoot
  ```

- 任何版本都可以选择万能的 `HEU_KMS_Activator` 来激活。

- 疑问：

  1. 为什么我的正版 `Windows10 中文版 21H2`， 安装之后直接就激活了，是因为 `Host` 宿主机是正版？  
  2. 但是该版本在普通电脑上安装，不会自动被激活。  
  3. `Windows10 中文版 22H2` 即使安装虚拟机也不会被自动激活。

### 3.2 激活方法-2

- 命令提示符（管理员）输入如下指令：

  ```shell
  slmgr /skms kms.v0v.bid && slmgr /ato
  ```

  ※ *大部分情况下，你能下载到的系统镜像都是VOL版（Business版）仅需以上一步即可成功激活。*

  查询激活情况：

  ```shell
  ## 查看系统版本（备用）
  wmic os get caption
  ## 查看激活详情（备用）
  slmgr /dlv
  slmgr /xpr
  ## 查看所有命令（备用）
  slmgr
  ```

- Office激活方法  
  命令提示符（管理员）进入Office `OSPP.VBS`目录  
  以 LTSC2021 64位版本为例，默认安装目录是  
  
  ```
  C:\Program Files\Microsoft Office\Office16
  ```
  
  打开命令提示符(管理员)执行以下命令进入`OSPP.VBS`目录
  
  执行 `slmgr /skms kms.v0v.bid && slmgr /ato` 命令激活Office软件
  
  ```
  cscript ospp.vbs /sethst:kms.v0v.bid && cscript ospp.vbs /act
  ```
  
  大部分情况下 你能下载到的安装包都是VOL版 仅需以上两步即可成功激活。
  
  ```bash
  ## 查询Office激活详情：
  cscript ospp.vbs /dstatus
  ## 查看所有命令（备用）
  cscript ospp.vbs
  ```
  
  批处理文件（命令提示符的管理员权限）：
  
  ```bash
  C:
  CD "C:\Program Files\Microsoft Office\Office16"
  cscript ospp.vbs /sethst:kms.v0v.bid && cscript ospp.vbs /act
  ```
  
  

### 3.3 KMS 激活码

- 微软官方提供 KMS激活办法：  
  https://learn.microsoft.com/zh-cn/windows-server/get-started/kms-client-activation-keys?tabs=server2025%2Cwindows1110ltsc%2Cversion1803%2Cwindows81  
  浏览器搜索 `KMS 客户端激活`，即可找到如上官方网址。

### 3.4 直接激活

- 执行如下指令：

  ```bash
  slmgr /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
  slmgr /skms kms8.msguides.com
  
  #这一步可能需要重复执行，直至显示激活成功#
  slmgr /ato 
  ```

- Windows 11许可密钥列表：

  ```
  家庭版：
  TX9XD-98N7V-6WMQ6-BX7FG-H8Q99
  3KHY7-WNT83-DGQKR-F7HPR-844BM
  7HNRX-D7KGG-3K4RQ-4WPJ4-YTDFH
  PVMJN-6DFY6-9CCP6-7BKTT-D3WVR
  专业版：
  W269N-WFGWX-YVC9B-4J6C9-T83GX
  MH37W-N47XK-V7XM9-C7227-GCQG9
  教育版：
  NW6C2-QMPVW-D7KKK-3GKT6-VCFB2
  2WH4N-8QGBV-H22JP-CT43Q-MDWWJ
  NPPR9-FWDCX-D2C8J-H872K-2YT43
  DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4
  ```

## 4. 安装 `winget`

> 官方网站：https://learn.microsoft.com/zh-cn/windows/package-manager/winget/

偶尔发现用 `Microsoft Store` 中输入 `winget` 居然找不到该软件：

- 中文名称：`应用安装程序`

- 日文名称：`アプリ インストーラー`

- 网页地址：

  https://apps.microsoft.com/detail/9NBLGGH4NNS1?rtc=1&hl=ja-jp&gl=JP#activetab=pivot:overviewtab

应用该工具，可以卸载一些用不到的系统软件。

※ 安装这个工具，在速度较慢电脑上需要一些时间，虽然画面上显示已经安装，但是要等到弹窗显示安装结束才能够使用。

## 5. 系统自动登录

- `Windows 7`

  ```
  control userpasswords2
  ```

- `Windows 10`，`Windows 11`

  首先，修改注册表，把 `DevicePasswordLessBuildVersion` 的默认值 `2` 修改为 `0`

  ```
  HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\PasswordLess\Device
  ```

  然后执行如下任意的指令，和WIN7一样的办法修改

  ```
  control userpasswords2
  netplwiz
  ```

## 6. 修改浏览器的缓存路径

### 6.1. 修改EDGE缓存目录

前两种方法的优先级低于另外两个，而且可能会随着浏览器的更新而失效。

第三种策略组的方法初次配置时会比较麻烦，推荐第四种方法。也可将缓存目录放入`RamDisk`中来分担`SSD`的写入量，减轻`SSD`的寿命损耗。

#### 6.1.1 通过命令行标志

在Edge浏览器的快捷方式添加对应的参数，`--disk-cache-dir`（磁盘缓存目录），`--user-data-dir`（用户数据目录）。

右键点击快捷方式选择属性，在目标后添加，比如，添加参数 `--disk-cache-dir="D:\Cache"` 可以将磁盘缓存目录设置为`D:\Cache`。

目录不存在的话它会自动创建，需要注意目录最后不要输入多余的 \ ，如 D:\Cache\。可以同时添加多个参数，注意空格分隔。

#### 6.1.2 通过mklink命令

Edge默认缓存位置在`C:\Users\%USER%\AppData\Local\Microsoft\Edge\User Data\Default\Cache`。假设我们需要把缓存位置更改为`D:\Cache`。

先删除`C:\Users\%USER%\AppData\Local\Microsoft\Edge\User Data\Default\Cache`文件夹，在D盘新建文件夹，重命名为`Cache`。以管理员身份运行CMD，输入以下指令并回车运行 

```
mklink /d "C:\Users\Administrator\AppData\Local\Microsoft\Edge\User Data\Default\Cache" "D:\Cache" 
```

#### 6.1.3 通过注册表

在开始菜单或运行中输入 `regedit` 打开注册表编辑器，展开`HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft`，右键点击`Microsoft`，选择 `新建` - `项`，命名为`Edge`。如果存在则无需创建。

右键点击`Edge`，选择 `新建` - `字符串值`，命名如下：

`DiskCacheDir` - 设置磁盘缓存目录

`UserDataDir` - 设置用户数据目录

然后双击修改其数据为指定目录。

```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Edge]
"DiskCacheDir"="D:\\EdgeCache\\Disk"
"UserDataDir"="D:\\EdgeCache\\User"
```

### 6.2 修改Chrome缓存目录

#### 6.2.1 通过命令行标志

这个办法比较简单、清晰，但是可能因为更新需要重新设置。

- 修改之前：

  ```
  "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --allow-file-access-from-files
  ```

- 修改之后：

  指令的后面追加 `--disk-cache-dir=R:\ChromeCache`，修改如下：

  ```
  "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --allow-file-access-from-files --disk-cache-dir=R:\ChromeCache
  ```

  

#### 6.2.2 通过mklink命令

- 查找缓存路径

  在 `Chrome` 的URL栏里面输入 `chrome://Version`。路径为 `C:\Users\KST_LIUZHIJUN\AppData\Local\Google\Chrome\User Data\Default`

- 删除缓存路径

- 建立新的路径

  例：`R:\ChromeCache`

- 管理员权限打开命令提示符，输入如下指令：

  ```
  Mklink /d "C:\Users\KST_LIUZHIJUN\AppData\Local\Google\Chrome\User Data\Default\Cache" "R:\ChromeCase"
  ```

  

## 7. 创建RAM虚拟硬盘

> https://beebom.com/create-ram-disk-windows-10-super-fast-read-write-speeds/

- 工具软件一览：

  | 软件                       | 是否免费      | 备注                                                         |
  | -------------------------- | ------------- | ------------------------------------------------------------ |
  | ImDisk                     | FREE          | 在使用之前，不会直接占用内存，相对比较友好。<br />ImDisk 是一款功能強大且開放原始碼的虛擬光碟機軟體，其中也有提供RAMDisk功能，支援動態記憶體管理、映像檔同步、關機回存、有極佳的I/O 效能，介面僅有英文，軟體是使用服務載入，不能放置分頁檔。 |
  | Screenshots                | Licence       |                                                              |
  | Primo Ramdisk              | Licence       | 业界翘楚<br />由Romexsoftware所推出的RAMDisk軟體，VSuite RamDisk是Primo RAMDisk的前身，在改版前有提供免費版，改版後所推出的版本皆須付費，提供許多功能，幾乎是所有RAMDisk軟體中功能最多最完善的，軟體是驅動加載所以可以放置分頁檔 pagefile.sys，支援繁體中文，也是我目前使用的RAMDisk軟體。 |
  | SoftPerfect RAMDisk        | Licence       | 4.0版本後不在推出免費版                                      |
  | Ultra RAMDisk              | 付費版&免費版 | 免費版僅可新增一個2GB的RAMDisk，且不可使用動態記憶體分配功能 |
  | AMD Radeon™ RAMDisk        | 付費版&免費版 | 由AMD與Dataram合作所推出的Radeon RAMDisk軟體，操作介面與Dataram RAMDisk完全一樣，有為私人非商業用途提供免費版，免費版最多建立4GB的磁碟區，對一般使用而言已經足夠使用，如果需要更大的空間就需購買付費的版本，軟體是驅動加載所以可以放置分頁檔 pagefile.sys （較可惜的是只有英文版）。 |
  | Asus ROG RAMDisk           | 限制          | 由ASUS為高端ROG系列主機板推出的RAMDisk軟體，其他主機板品牌也有類似的軟體，特別舉出來的原因是不僅非ROG系列的主機版可用，甚至其他品牌的主機板也可使用，支持多國語言，介面簡單易懂，官方不建議在`ROG RAMDisk`中放置分頁檔文件以及啟動資料夾。 |
  | DAEMON Tools Ultra RAMDisk | 付費版        | RAMDisk功能僅在付費版的DAEMON Tools Ultra版本中有提供，支援多個虛擬磁碟、映像檔同步關機回存功能。 |
  | Dataram Ramdisk            |               | 是由Dataram所推出的RAMDisk軟體，有為私人非商業用途提供免費版，在過去免費版最多可建立4GB的磁碟區，不過目前的版本僅能新增1023MB，如果需要用到4GB，建議可以使用前篇介紹的AMD與Dataram合作推出的AMD Radeon™ RAMDisk，功能、介面完全一模一樣，軟體是驅動加載所以可以放置分頁檔 pagefile.sys（只有英文版）。 |



**使用 RAM 磁盘的的理由**

- 更高的计算机性能：

  如果您有足够的 `RAM`，使用快速内存磁盘存储临时数据将提高计算机性能。

- 减少物理磁盘的磨损：

  由于临时文件不会写入硬盘，因此读写周期会减少，这对于延长笔记本电脑中常用的固态硬盘 (SSD) 的使用寿命尤其重要。

- 硬盘上的垃圾更少：

  许多软件应用程序会创建临时文件，这些文件虽然不再需要，但仍保持未删除状态。 每次计算机重新启动或关闭时，RAM 磁盘的内容都会被清除，因此不需要的文件不会弄乱您的硬盘。

- 减少文件系统碎片：

  HDD/SSD 上的文件系统碎片将大大减少，因为临时文件永远不会写入硬盘。

- 增强安全性：

  临时敏感数据可以存储在 `RAM` 磁盘上，当计算机关闭或重新启动时，这些数据将被完全删除。

### 7.1 `ImDisk`

> https://sourceforge.net/projects/imdisk-toolkit/

## 8. 删除恢复分区

尤其是虚拟机，本来就是临时用的，完全没有保留 `恢复分区` 的必要，这是给普通用户预留的功能。

管理员权限启动 `命令提示符`，按照如下命令进行操作：

1. 查找 `系统恢复分区`

   ```bash
   reagentc /info
   ```

   能够看到 `Windows PE 位置` 自然的内容，那就是 系统恢复分区 的存放分区。

2. 禁用任何映射到联机映像的活动 Windows RE 映像

   ```
   reagentc /disable
   ```

   操作中没有使用该指令，也没有发现问题。`系统恢复分区` 被删除之后，自动会变为 `disabled` 状态。

   指令详细请参照 [官方说明](https://learn.microsoft.com/zh-cn/windows-hardware/manufacture/desktop/reagentc-command-line-options?view=windows-11)

3. 如下指令删除指定分区

   ```bash
   ## 启动磁盘工具
   diskpart
   ## 列表所有硬盘
   list disk
   ## 选择指定硬盘（#指的是包含恢复分区的磁盘驱动器号）
   select disk #
   ## 列表所有分区（将所选磁盘上的卷及其标签全部列出）
   list part
   ## 选择伤处目标硬盘（#指的是这次要删除的系统恢复分区的编号）
   select partition #
   ## 删除选定分区
   delete partition override
   ## 退出磁盘工具
   exit
   ```

   不能用有些网页介绍的 `list volume` `select volume #` `delete volume` 等指令，系统会警告 `没有权限` 导致无法删除分区。

4. 删除后，可以在 Windows管理 的 `磁盘管理` 内，把空余出来的空间纳入系统盘部分。

   虚拟机的话，可以运用 瘦身 策略，回收 系统恢复分区 被删除之后的的硬盘占用空间。

## 9. 系统更新失败

- 曾经 `KB4023057` 的Windows10更新，一直无法成功（错误 0x80070643），执行如下处理后，可以正常更新。

  `2024-01 x64 ベース システム用 Windows 10 Version 22H2 のセキュリティ更新プログラム (KB5034441)`

  ```bat
  net stop wuauserv
  net stop cryptSvc
  net stop bits
  net stop msiserver
  Ren C:\Windows\SoftwareDistribution SoftwareDistribution.old
  Ren C:\Windows\System32\catroot2 Catroot2.old
  net start wuauserv
  net start cryptSvc
  net start bits
  net start msiserver
  
  ```

  也可以尝试如下办法：

  > 1. windows+R后，输入services.msc，回车
  > 2. 找到Windows Update，手动停掉
  > 3. 定位到C:\Windows\Software Distribution，清掉datastore里的内容
  > 4. 重启Windows Update，重新检查更新。

 ## Z. Windows系统变量

### 系统变量信息

| 项目                                 | 变量                                                         | 备注 |
| ------------------------------------ | ------------------------------------------------------------ | ---- |
| 当前系统盘符                         | %systemdrive%或%HomeDrive% = C:\                             |      |
| 当前系统目录                         | %systemroot%或%Windir%=C:\WINDOWS                            |      |
| 当前用户配置文件文件夹               | %UserProfile%或%HOMEPATH%=C:\Documents and Settings\Administrator\ |      |
| 所有用户配置文件文件夹               | %AllUsersProfile%=C:\Documents and Settings\All Users\       |      |
| 临时文件夹之一 当前用户Temp缓存      | %temp% = %USERPROFILE%\Local Settings\Temp =C:\Documents and Settings\Administrator\Local Settings\Temp\ |      |
| 临时文件夹之二 系统Temp缓存          | %SystemRoot%\TEMP\=C:\WINDOWS\Temp\                          |      |
| 程序文件夹                           | %ProgramFiles%=C:\Program Files\                             |      |
| 包含用户帐户的域的名称               | %USERDOMAIN%                                                 |      |
| 当前登录的用户的名称                 | %USERNAME%=Administrator                                     |      |
| 用户桌面                             | %USERPROFILE%\桌面=C:\Documents and Settings\Administrator\桌面 |      |
| 本地默认情况下应用程序存储数据的位置 | %APPDATA%=C:\Documents and Settings\Administrator\Application Data |      |
| 显示当前目录                         | %CD%=C:\Documents and Settings\Administrator                 |      |
| 启动当前的 Cmd.exe 的准确命令行      | %CMDCMDLINE%                                                 |      |
| 当前的“命令处理程序扩展”的版本号     | %CMDEXTVERSION%                                              |      |
| 计算机的名称                         | %COMPUTERNAME%                                               |      |
| 命令行解释器可执行程序的准确路径     | %COMSPEC%                                                    |      |
| 当前日期                             | %DATE%                                                       |      |
| 当前时间（精确到毫秒）               | %TIME%                                                       |      |
| 上一条命令的错误代码                 | %ERRORLEVEL%                                                 |      |
| 验证当前登录会话的域控制器的名称     | %LOGONSERVER%                                                |      |
| 安装在计算机上的处理器的数目         | %NUMBER_OF_PROCESSORS%                                       |      |
| 操作系统名称                         | %OS%                                                         |      |

 ### 其它变量信息

| 项目                                      | 变量                     | 备注 |
| ----------------------------------------- | ------------------------ | ---- |
| 指定可执行文件的搜索路径                  | %PATH%                   |      |
| 操作系统认为可执行的文件扩展名的列表      | %PATHEXT%                |      |
| 处理器的芯片体系结构                      | %PROCESSOR_ARCHITECTURE% |      |
| 处理器说明                                | %PROCESSOR_IDENTIFIER%   |      |
| 计算机上安装的处理器的型号                | %PROCESSOR_LEVEL%        |      |
| 处理器的版本号                            | %PROCESSOR_REVISION%     |      |
| 当前解释程序的命令提示符设置              | %PROMPT%                 |      |
| 0 到 32767 之间的任意十进制数字（随机数） | %RANDOM%                 |      |

以上面的假设，`%windir%`实际就是 `C:\windows`  
如果对`%%`括起来的系统变量不清楚的话，可点击开始按钮，打开运行→输入cmd→在dos命令提示符窗口下输入: `echo %windir%` 即可得到答案。

