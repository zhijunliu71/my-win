# 永久固定U盘盘符

> 在拷贝数据的时候，一会儿移动硬盘、一会用U盘，盘符会随着盘符数量改变，如果能固定U盘盘符就再好不过了。  
> 本人电脑的 E、F、G属于虚拟驱动器，是从 D盘的子目录虚拟出来的。如果启机的时候，U盘设备快速抢占了虚拟的 E盘 的话，会非常麻烦，所以需要该问题。

1. 首先在开始菜单输入CMD，然后右键单击选择“以管理员身份运行”。
2. 输入命令 **diskpart** 进入磁盘管理命令模式。
3. 这时，输入 **list volume** 命令列出当前所有的驱动器盘符。
4. 然后选中要固定的盘符设备，比如小编的U盘卷标是4，那么就输入命令 **select volume 4** ，注意，这里只选择卷标，可不是盘符！
5. 这个时候，为了让U盘永久固定，就需要用命令强制固定一个U盘盘符，这样以后无论什么时候、什么情况下插入U盘，都会被固定在这个盘符上了。

   具体的命令为 **assign letter=z** ，其中z是小编指定的U盘盘符，这个可以根据自己喜好更改。
6. 最后，输入exit退出，然后关闭窗口。这时U盘已经变更成指定好的盘符了，以后再插入这个U盘，也会固定在这个盘符上，再也不会乱跑了。



- 命令处理处理过程：

  ```shell
  Microsoft Windows [Version 10.0.19045.2311]
  (c) Microsoft Corporation. All rights reserved.
  
  C:\WINDOWS\system32>diskpart
  
  Microsoft DiskPart バージョン 10.0.19041.964
  
  Copyright (C) Microsoft Corporation.
  コンピューター: DESKTOP-DOAA0KK
  
  DISKPART> list volume
  
    Volume ###  Ltr Label        Fs    Type        Size     Status     Info
    ----------  --- -----------  ----  ----------  -------  ---------  --------
    Volume 0     D                NTFS   Partition    953 GB  正常
    Volume 1         システムで予約済み    NTFS   Partition    579 MB  正常         システム
    Volume 2     C                NTFS   Partition    464 GB  正常         ブート
    Volume 3                      NTFS   Partition    654 MB  正常         非表示
    Volume 4     H   ボリューム        NTFS   Partition    447 GB  正常
  
  DISKPART> select volume 4
  
  ボリューム 4 が選択されました。
  
  DISKPART> assign letter=h
  
  DiskPart はドライブ文字またはマウント ポイントを正常に割り当てました。
  
  DISKPART> exit
  ```

## 文件夹镜像成本地磁盘

Windows - subst命令创建虚拟磁盘

- 命令程序所在路径：

  C:\WINDOWS\system32\subst.exe

- 创建虚拟驱动器：

  ```shell
  subst Z: F:\QQDownload
  ```

- 列出虚拟驱动器：

  ```shell
  subst
  ```
  
  ※在Windows10 22H2 版本，该指令无效，曾经记得过去的版本是有效果的。

- 删除虚拟驱动器：

  ```shell
  subst Z: /d
  ```

  